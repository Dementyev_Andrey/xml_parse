import fetch from 'node-fetch';
import fs, { writeFileSync } from 'fs'
import AdmZip from "adm-zip"
import iconv from 'iconv-lite'
import parseString from 'xml2js'



export async function download(url)  {
  const response = await fetch(url);
  const buffer = await response.buffer();
  writeFileSync('test.zip', buffer);
  console.log('Done!');
};

function remove(path) {
  fs.unlink(path, (err) => {
    if (err) {
        throw err;
    }
    console.log("Delete File successfully.");
  });
};

export function extract()  {
    var zip = new AdmZip("./test.zip");
    zip.extractAllTo("./for_parse", true);
    remove('./test.zip')
};

export async function parse() {
  let xml_file = fs.readdirSync('./for_parse')[0]
  let fileContent = fs.readFileSync('./for_parse/'+xml_file)
  const ps = parseString.parseString
  let encodedData = iconv.decode(fileContent, 'cp1251')

  let response=[];
  let res;
  ps(encodedData, function (err, result) {
      res = result
    });
  res['ED807']['BICDirectoryEntry'].forEach(element => {
      if (element['Accounts'] != undefined) {
          element['Accounts'].forEach(account => {
              let obj = {
                  'bic': element['$']['BIC'],
                  'name': element['ParticipantInfo'][0]['$']['NameP'],
                  'corrAccount': account['$']['Account']
              }  
              response.push(obj)
          })
       }
  });
  let path = './for_parse/' + xml_file
  remove(path);
  return response
};